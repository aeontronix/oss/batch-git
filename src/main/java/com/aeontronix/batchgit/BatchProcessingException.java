package com.aeontronix.batchgit;

public class BatchProcessingException extends RuntimeException {
    public BatchProcessingException() {
    }

    public BatchProcessingException(String message) {
        super(message);
    }

    public BatchProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public BatchProcessingException(Throwable cause) {
        super(cause);
    }

    public BatchProcessingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
