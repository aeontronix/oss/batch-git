package com.aeontronix.batchgit;

import java.util.List;

public class BatchFilterResult {
    private List<String> file;

    public BatchFilterResult(List<String> file) {
        this.file = file;
    }

    public List<String> getFile() {
        return file;
    }
}
