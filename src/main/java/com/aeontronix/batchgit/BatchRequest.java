package com.aeontronix.batchgit;

import java.util.List;

public class BatchRequest {
    private String name;
    private String workBranch;
    private String commitMessage;
    private List<Backend> backends;
    private List<BatchOperation> operations;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkBranch() {
        return workBranch;
    }

    public void setWorkBranch(String workBranch) {
        this.workBranch = workBranch;
    }

    public String getCommitMessage() {
        return commitMessage;
    }

    public void setCommitMessage(String commitMessage) {
        this.commitMessage = commitMessage;
    }

    public List<Backend> getBackends() {
        return backends;
    }

    public void setBackends(List<Backend> backends) {
        this.backends = backends;
    }

    public List<BatchOperation> getOperations() {
        return operations;
    }

    public void setOperations(List<BatchOperation> operations) {
        this.operations = operations;
    }
}
