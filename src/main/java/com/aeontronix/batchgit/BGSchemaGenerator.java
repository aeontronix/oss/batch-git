package com.aeontronix.batchgit;

import com.aeontronix.commons.file.FileUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.github.victools.jsonschema.generator.*;
import com.github.victools.jsonschema.module.jackson.JacksonModule;

import java.io.File;
import java.io.IOException;

public class BGSchemaGenerator {
    public static void main(String[] args) throws IOException {
        final File schema = new File(args[0]);
        SchemaGeneratorConfigBuilder configBuilder = new SchemaGeneratorConfigBuilder(SchemaVersion.DRAFT_2019_09, OptionPreset.PLAIN_JSON);
        SchemaGeneratorConfig config = configBuilder.with(new JacksonModule()).build();
        SchemaGenerator generator = new SchemaGenerator(config);
        JsonNode jsonSchema = generator.generateSchema(BatchRequest.class);
        FileUtils.write(schema, jsonSchema.toPrettyString());
    }
}
