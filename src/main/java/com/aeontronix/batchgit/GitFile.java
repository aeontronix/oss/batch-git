package com.aeontronix.batchgit;

import java.io.IOException;

public interface GitFile {
    String getPath();

    byte[] getContent() throws IOException;

    void writeContent(byte[] content) throws IOException;
}
