package com.aeontronix.batchgit.filter;

import com.aeontronix.batchgit.BatchFilter;
import com.aeontronix.batchgit.GitFile;
import com.aeontronix.batchgit.GitRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PathBatchFilter implements BatchFilter {
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public List<GitFile> filter(@NotNull GitRepository repository, @Nullable List<GitFile> files) {
        List<GitFile> filtered = new ArrayList<>();
        if (files == null) {
            files = repository.listFiles();
        }
        return files.stream().filter(gitFile -> gitFile.getPath().matches(path)).collect(Collectors.toList());
    }
}
