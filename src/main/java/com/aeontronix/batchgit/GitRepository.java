package com.aeontronix.batchgit;

import java.util.List;
import java.util.Set;

public interface GitRepository {
    String getId();
    List<GitFile> listFiles();

    void branch(String branchName);

    void commit(String commitComment);

    void push();
}
