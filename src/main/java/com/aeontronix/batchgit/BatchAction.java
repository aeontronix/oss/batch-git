package com.aeontronix.batchgit;

import com.aeontronix.batchgit.action.RegexTransformBatchAction;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.List;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = RegexTransformBatchAction.class, name = "regex")
})
public interface BatchAction {
    void execute(List<GitFile> files);
}
