package com.aeontronix.batchgit;

import com.aeontronix.batchgit.filter.PathBatchFilter;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = PathBatchFilter.class, name = "path")
})
public interface BatchFilter {
    List<GitFile> filter(@NotNull GitRepository repository, @Nullable List<GitFile> files);
}
