package com.aeontronix.batchgit;

import com.aeontronix.commons.io.IOUtils;
import com.fasterxml.jackson.databind.InjectableValues;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BatchProcess {
    private final BatchRequest request;
    private final File workDir;
    private final Map<String,GitCredential> credentials = new HashMap<>();
    private final Map<GitRepository, Map<BatchOperation, List<GitFile>>> state = new HashMap<>();
    private final ObjectMapper objectMapper;

    public BatchProcess(URL url, File workDir) throws IOException {
        objectMapper = new ObjectMapper();
        objectMapper.setInjectableValues(new InjectableValues.Std().addValue(BatchProcess.class,this));
        this.request = loadRequest(url);
        this.workDir = workDir;
    }

    public File getWorkDir() {
        return workDir;
    }

    public BatchRequest loadRequest(URL url) throws IOException {
        BatchRequest request = objectMapper.readValue(url, BatchRequest.class);
        if (request.getName() == null) {
            String file = url.getFile();
            final int idx = file.lastIndexOf("/");
            if (idx != -1) {
                file = file.substring(idx + 1);
            }
            request.setName(file);
        }
        return request;
    }

    public synchronized void init() {
        for (Backend backend : request.getBackends()) {
            final GitCredential gitCredential = credentials.get(backend.getId());
            if( gitCredential != null ) {
                backend.setCredentials(gitCredential);
            }
        }
    }

    public synchronized void filter() {
        state.clear();
        List<GitRepository> repositories = request.getBackends().stream().flatMap(backend -> backend.findRepositories().stream())
                .collect(Collectors.toList());
        for (GitRepository repository : repositories) {
            for (BatchOperation operation : request.getOperations()) {
                List<GitFile> files = null;
                for (BatchFilter filter : operation.getFilters()) {
                    files = filter.filter(repository, files);
                }
                state.computeIfAbsent(repository, r -> new HashMap<>()).put(operation, files != null ? files : Collections.emptyList());
            }
        }
    }

    public synchronized void setBackendCredential(String backendId, GitCredential credential) {
        credentials.put(backendId,credential);
    }

    public void apply() {
        for (Map.Entry<GitRepository, Map<BatchOperation, List<GitFile>>> repoEntry : state.entrySet()) {
            final GitRepository repo = repoEntry.getKey();
            repo.branch(request.getWorkBranch());
            for (Map.Entry<BatchOperation, List<GitFile>> opEntry : repoEntry.getValue().entrySet()) {
                final BatchOperation operation = opEntry.getKey();
                final List<GitFile> files = opEntry.getValue();
                for (BatchAction action : operation.getActions()) {
                    action.execute(files);
                }
            }
            repo.commit(request.getCommitMessage());
            repo.push();
            IOUtils.close(repo);
        }
    }
}
