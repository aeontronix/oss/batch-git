package com.aeontronix.batchgit;

import java.util.List;

public class BatchOperation {
    private List<BatchFilter> filters;
    private List<BatchAction> actions;

    public List<BatchFilter> getFilters() {
        return filters;
    }

    public void setFilters(List<BatchFilter> filters) {
        this.filters = filters;
    }

    public List<BatchAction> getActions() {
        return actions;
    }

    public void setActions(List<BatchAction> actions) {
        this.actions = actions;
    }
}
