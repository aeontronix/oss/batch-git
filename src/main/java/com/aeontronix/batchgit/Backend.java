package com.aeontronix.batchgit;

import com.aeontronix.batchgit.backend.BitBucketV1Backend;
import com.aeontronix.batchgit.backend.GitBackend;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.List;
import java.util.Set;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = GitBackend.class, name = "git"),
        @JsonSubTypes.Type(value = BitBucketV1Backend.class, name = "bitbucketv1"),
})
public abstract class Backend {
    @JsonProperty( required = true )
    protected String id;
    @JsonIgnore
    protected GitCredential gitCredential;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public abstract Set<GitRepository> findRepositories();

    public void setCredentials(GitCredential gitCredential) {
        this.gitCredential = gitCredential;
    }
}
