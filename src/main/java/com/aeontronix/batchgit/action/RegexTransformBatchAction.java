package com.aeontronix.batchgit.action;

import com.aeontronix.batchgit.BatchAction;
import com.aeontronix.batchgit.BatchProcessingException;
import com.aeontronix.batchgit.GitFile;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;
import java.util.List;

public class RegexTransformBatchAction implements BatchAction {
    private String matcher;
    private String replace;
    @JsonProperty(defaultValue = "UTF-8")
    private String encoding = "UTF-8";

    public String getMatcher() {
        return matcher;
    }

    public void setMatcher(String matcher) {
        this.matcher = matcher;
    }

    public String getReplace() {
        return replace;
    }

    public void setReplace(String replace) {
        this.replace = replace;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    @Override
    public void execute(List<GitFile> files) {
        try {
            for (GitFile file : files) {
                byte[] content = file.getContent();
                file.writeContent(new String(content,encoding).replace(matcher,replace).getBytes(encoding));
            }
        } catch (IOException e) {
            throw new BatchProcessingException(e);
        }
    }
}
