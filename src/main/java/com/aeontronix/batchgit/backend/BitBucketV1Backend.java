package com.aeontronix.batchgit.backend;

import com.aeontronix.batchgit.Backend;
import com.aeontronix.batchgit.GitRepository;

import java.util.Collections;
import java.util.Set;

public class BitBucketV1Backend extends Backend {
    private String serverUrl;
    private Set<String> repositories;
    private boolean regex;

    public BitBucketV1Backend() {
    }

    public BitBucketV1Backend(String serverUrl, Set<String> repositories, boolean regex) {
        this.serverUrl = serverUrl;
        this.repositories = repositories;
        this.regex = regex;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public Set<String> getRepositories() {
        return repositories;
    }

    public void setRepositories(Set<String> repositories) {
        this.repositories = repositories;
    }

    public boolean isRegex() {
        return regex;
    }

    public void setRegex(boolean regex) {
        this.regex = regex;
    }

    @Override
    public Set<GitRepository> findRepositories() {
        return Collections.emptySet();
    }
}
