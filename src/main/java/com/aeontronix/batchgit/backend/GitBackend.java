package com.aeontronix.batchgit.backend;

import com.aeontronix.batchgit.Backend;
import com.aeontronix.batchgit.BatchProcess;
import com.aeontronix.batchgit.GitRepository;
import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;
import java.util.stream.Collectors;

public class GitBackend extends Backend {
    @JacksonInject
    private BatchProcess batchProcess;
    @JsonProperty(required = true)
    private String baseUrl;
    private Set<String> repositories;
    @JsonProperty(defaultValue = ".git")
    private String postFix = ".git";

    public GitBackend() {
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Set<String> getRepositories() {
        return repositories;
    }

    public void setRepositories(Set<String> repositories) {
        this.repositories = repositories;
    }

    public String getPostFix() {
        return postFix;
    }

    public void setPostFix(String postFix) {
        this.postFix = postFix;
    }

    @Override
    public Set<GitRepository> findRepositories() {
        return repositories.stream().map(s -> new GitBackendRepository(gitCredential, batchProcess.getWorkDir(), baseUrl + s + postFix, s))
                .collect(Collectors.toSet());
    }
}
