package com.aeontronix.batchgit.backend;

import com.aeontronix.batchgit.GitFile;
import com.aeontronix.commons.file.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class GitBackendFile implements GitFile {
    private GitBackendRepository repository;
    private File file;
    private String path;

    public GitBackendFile(GitBackendRepository gitBackendRepository, File file, Path path) {
        repository = gitBackendRepository;
        this.file = file;
        this.path = File.separatorChar == '/' ? path.toString() : path.toString().replace(File.separatorChar,'/');
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public byte[] getContent() throws IOException {
        return FileUtils.toByteArray(file);
    }

    @Override
    public void writeContent(byte[] content) throws IOException {
        FileUtils.write(file, content);
        repository.add(path);
    }
}
