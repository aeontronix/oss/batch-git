package com.aeontronix.batchgit.backend;

import com.aeontronix.batchgit.*;
import com.aeontronix.commons.file.FileUtils;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class GitBackendRepository implements GitRepository, Closeable {
    private File dir;
    private GitCredential gitCredential;
    private String uri;
    private String id;
    private Git gitClone;

    public GitBackendRepository(GitCredential gitCredential, File workingDir, String uri, String id) {
        this.gitCredential = gitCredential;
        this.uri = uri;
        this.id = id;
        dir = new File(workingDir, id);
    }

    @Override
    public void close() throws IOException {
        if (gitClone != null) {
            gitClone.close();
        }
    }

    public synchronized void cloneRepo() {
        try {
            if (gitClone == null) {
                if (dir.exists()) {
                    FileUtils.delete(dir);
                }
                gitClone = gitCredential.init(Git.cloneRepository()).setURI(uri)
                        .setDirectory(dir).call();
            }
        } catch (GitAPIException | IOException e) {
            throw new BatchProcessingException(e);
        }
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public synchronized List<GitFile> listFiles() {
        cloneRepo();
        List<File> list = new ArrayList<>();
        LinkedList<File> queue = new LinkedList<>(Arrays.asList(Objects.requireNonNull(dir.listFiles())));
        while( ! queue.isEmpty() ) {
            final File file = queue.removeFirst();
            if( file.isDirectory() && !file.getName().equals(".git") ) {
                queue.addAll(Arrays.asList(Objects.requireNonNull(file.listFiles())));
            } else {
                list.add(file);
            }
        }
        return list.stream().map(file -> new GitBackendFile(this,file,dir.toPath().relativize(file.toPath()))).collect(Collectors.toList());
    }

    @Override
    public void branch(String branchName) {
        try {
            gitClone.branchCreate().setName(branchName).call();
        } catch (GitAPIException e) {
            throw new BatchProcessingException(e);
        }
    }

    @Override
    public void commit(String commitMessage) {
        try {
            gitClone.commit().setMessage(commitMessage).call();
        } catch (GitAPIException e) {
            throw new BatchProcessingException(e);
        }
    }

    @Override
    public void push() {
        try {
            gitCredential.init(gitClone.push()).call();
        } catch (GitAPIException e) {
            throw new BatchProcessingException(e);
        }
    }

    public void add(String path) {
        try {
            gitClone.add().addFilepattern(path).call();
        } catch (GitAPIException e) {
            throw new BatchProcessingException(e);
        }
    }
}
