package com.aeontronix.batchgit;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import org.eclipse.jgit.api.TransportCommand;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.Transport;
import org.eclipse.jgit.transport.ssh.jsch.JschConfigSessionFactory;

import static java.nio.charset.StandardCharsets.UTF_8;

public class GitSSHCredential implements GitCredential {
    private String privateKey;
    private String publicKey;

    public GitSSHCredential(String privateKey, String publicKey) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    public GitSSHCredential() {
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    @Override
    public <X extends TransportCommand<?, ?>> X init(X command) {
        command.setTransportConfigCallback(new TransportConfigCallback() {
            @Override
            public void configure(Transport transport) {
                if (transport instanceof SshTransport) {
                    ((SshTransport) transport).setSshSessionFactory(new JschConfigSessionFactory() {
                        @Override
                        protected void configureJSch(JSch jsch) {
                            try {
                                jsch.addIdentity("sshKey", privateKey.getBytes(UTF_8), publicKey.getBytes(UTF_8), null);
                            } catch (JSchException e) {
                                throw new BatchProcessingException(e);
                            }
                            super.configureJSch(jsch);
                        }
                    });
                }
            }
        });
        return command;
    }
}
