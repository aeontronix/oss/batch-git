package com.aeontronix.batchgit;

import org.eclipse.jgit.api.GitCommand;
import org.eclipse.jgit.api.TransportCommand;

public interface GitCredential {
    <X extends TransportCommand<?,?>> X init(X command);
}
