package com.aeontronix.batchgit;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URL;

public class BatchTests {
    @Test
    public void test1() throws Exception {
        final URL resource = BatchTests.class.getResource("/test1.json");
        final BatchProcess p = new BatchProcess(resource, new File("target/workdir"));
        p.setBackendCredential("gh",new GitSSHCredential("-----BEGIN OPENSSH PRIVATE KEY-----\n" +
                "b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn\n" +
                "NhAAAAAwEAAQAAAYEAy6lD2S98QFHjPhJTRJ7SGBlCL3vtqCYTDcgNgrm4vXccuZGKgDyM\n" +
                "tkeuQkrOJ3HVVtJ6pVOZrFQXqoM8MBJivpW18qky0EN7gJl8JpPUAKQR/oAlJp3Rqiviur\n" +
                "GcqAFdhbkdKu5wWEy+kjGtL4VcAVIdyXmjIJdPflqpLqXTuF4UUS7iyEO0jOObY4XrPx+n\n" +
                "oRRO6VvAG7rXZeKcnpZhA6SDMLXZkhp3qkLUz1J3CJtm7FImj+A16GB07jOdUdJra9ggOY\n" +
                "3fjv0bfvJ7DoupoOnXrgnmjf0p8dy5jvJnSrrGBzhEdSmXEEwAT6/5V4bvNSmAZFsMXG6J\n" +
                "p8CzTYd18pVP94keDLg0gPtVnkt+zrEzYl+QAPcjSneOWhQOtEICAfkNpz8Y2H/2HiP6g9\n" +
                "igGmuanZg8lPG0uN0RQksF4Cs3FSIlLYfd9CH6rQXQoLaWWG7TZkBTL7rpkz4jb2ov/7iH\n" +
                "oksBIsZK/Mm6cIMoBKSwlp1D+nmNjLlmHzYISWvVAAAFkN1qUkfdalJHAAAAB3NzaC1yc2\n" +
                "EAAAGBAMupQ9kvfEBR4z4SU0Se0hgZQi977agmEw3IDYK5uL13HLmRioA8jLZHrkJKzidx\n" +
                "1VbSeqVTmaxUF6qDPDASYr6VtfKpMtBDe4CZfCaT1ACkEf6AJSad0aor4rqxnKgBXYW5HS\n" +
                "rucFhMvpIxrS+FXAFSHcl5oyCXT35aqS6l07heFFEu4shDtIzjm2OF6z8fp6EUTulbwBu6\n" +
                "12XinJ6WYQOkgzC12ZIad6pC1M9SdwibZuxSJo/gNehgdO4znVHSa2vYIDmN3479G37yew\n" +
                "6LqaDp164J5o39KfHcuY7yZ0q6xgc4RHUplxBMAE+v+VeG7zUpgGRbDFxuiafAs02HdfKV\n" +
                "T/eJHgy4NID7VZ5Lfs6xM2JfkAD3I0p3jloUDrRCAgH5Dac/GNh/9h4j+oPYoBprmp2YPJ\n" +
                "TxtLjdEUJLBeArNxUiJS2H3fQh+q0F0KC2llhu02ZAUy+66ZM+I29qL/+4h6JLASLGSvzJ\n" +
                "unCDKASksJadQ/p5jYy5Zh82CElr1QAAAAMBAAEAAAGBAI/jEJp7IA/ZSvFpxQawJsL75X\n" +
                "Ti26925vNPqav4RvslElhQ6UdcgBWwYv9XFntrw0xJ/Hidre7Aos/HVFiqRLVd1GSTAeF+\n" +
                "K91Zkws2Wgvx3vyt0JGN4rmVs+Qu4/0Wqdp2RJM2To0z+1gSXMc8MxqDx6O2mnfGL1WX+/\n" +
                "uHESQq47PPqk3O99qqW/hefyvj02zMikv4lKuCv/etuPWaTQRj0IQzlnnQ1UanXO701kBw\n" +
                "erDwAWqY/70jmLlKIhXS0OsNzhcag02+/cNDHEZeGJt29LOy4gLwarpQ85DtsOzC7aU19I\n" +
                "0ey8VTFES/PStiYXJBEnE6UNetz+DtNkvEL6GDMBtQNPX/mcmr5Vzn5LWvpkbJ61ipyKcv\n" +
                "MnjRvdWxG4dIqdmNUc8wMr/Fs4tXytHfc7/CAo0JDLqomMSMndY2a20zphy46g/E/p4Lmu\n" +
                "8LRvkYY7/wx2hU65cuWvQyJqhXWIdMXIyOyJn/3yn2rpAmkxiyFGyntNt/xBHZL85dqQAA\n" +
                "AMAVZ1wgnZ8/09xaxy0VWbF/chjOThsCKiDhiC1ti11kFgOQsJaHXf9xIEE3GNJWIvVU2T\n" +
                "53jES9tM4nZSVv4lImK1N1UxEpyHZTs6mHPjb5/QCi7gqO/0otsfDPZVz+So/uaBTKrc+Q\n" +
                "VPJUJazhgUnuyUNiW3fhjA9SCNVdYop0zb/ECONj5Tj6Wrcz2EsyoTV3XnSLCx5RGS8+Wb\n" +
                "7C8ojVTYQA73Fu4j2aBba8OuNISibn7OE0LAgwqseyMYGvzpYAAADBAPD4pBWtgXkAMWL2\n" +
                "2Wi7C2MGPQuWmfiy215lX+Jnvq0kAYMTbtvAW3ePKWOsJCgFnL3OokdCZYxxOby8zBycJ0\n" +
                "NiCrdAdexF0TUJsapB1vhqJBOMUCjOghpzmyViyJle9hmiDbixW0xjY8g2+mTN4UwU+69W\n" +
                "azHIL5BTVJSyz5EfnRzDTDOSroiZmMzG0golJJ/wZPH7SsunuctjfyYMl/NXKFpaFhdPKC\n" +
                "V8QHVVkZ6Aynthq8ekXniJLfYzke6YOwAAAMEA2FzuBRDjFo+y7eZs0PXmV15mgRossX8W\n" +
                "fJQI89MWmvrnuTwZ8+8jZsM434thVEOPPJGRIJjxurXqButQk/NT30Z1PUY7yr7Xy4UaH7\n" +
                "s5eJf1im0E2PtXLA67uoNkKdIh4Jkp339bknKc7eFOGGkzUwUVGV95LFbycc+dWyDt2m2N\n" +
                "w7IxvFDRNBgN/xt10jwtWRcisfKF1gCFfUqXLhRuOZLfkgu5IrwY8J9adMgvKI2HCP3uCV\n" +
                "fQFdrJ9b0Qs9svAAAAGnlhbm5pY2tAWWFubmlja3MtTUJQLmxvY2Fs\n" +
                "-----END OPENSSH PRIVATE KEY-----","ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDLqUPZL3xAUeM+ElNEntIYGUIve+2oJhMNyA2Cubi9dxy5kYqAPIy2R65CSs4ncdVW0nqlU5msVBeqgzwwEmK+lbXyqTLQQ3uAmXwmk9QApBH+gCUmndGqK+K6sZyoAV2FuR0q7nBYTL6SMa0vhVwBUh3JeaMgl09+WqkupdO4XhRRLuLIQ7SM45tjhes/H6ehFE7pW8Abutdl4pyelmEDpIMwtdmSGneqQtTPUncIm2bsUiaP4DXoYHTuM51R0mtr2CA5jd+O/Rt+8nsOi6mg6deuCeaN/Snx3LmO8mdKusYHOER1KZcQTABPr/lXhu81KYBkWwxcbomnwLNNh3XylU/3iR4MuDSA+1WeS37OsTNiX5AA9yNKd45aFA60QgIB+Q2nPxjYf/YeI/qD2KAaa5qdmDyU8bS43RFCSwXgKzcVIiUth930IfqtBdCgtpZYbtNmQFMvuumTPiNvai//uIeiSwEixkr8ybpwgygEpLCWnUP6eY2MuWYfNghJa9U= yannick@Yannicks-MBP.local") );
        p.init();
        p.filter();
        p.apply();
    }
}
